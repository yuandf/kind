package com.kind.common.uitls;

import java.text.DecimalFormat;

public class GeoUtils {
	  /**
     * 计算两经纬度点之间的距离（单位：公里）
     * @param lng1  经度
     * @param lat1  纬度
     * @param lng2
     * @param lat2
     * @return
     */
    public static double getDistance(double lat1,double lng1,double lat2,double lng2){
    	DecimalFormat    df   = new DecimalFormat("######0.00000000");   
        double radLat1 = Math.toRadians(lat1);
        double radLat2 = Math.toRadians(lat2);
        double a = radLat1 - radLat2;
        double b = Math.toRadians(lng1) - Math.toRadians(lng2);
        double s = 2 * Math.asin(Math.sqrt(Math.pow(Math.sin(a / 2), 2) + Math.cos(radLat1)
                * Math.cos(radLat2) * Math.pow(Math.sin(b / 2), 2)));
        s = s * 6378137.0;// 取WGS84标准参考椭球中的地球长半径(单位:m)
        s = Math.round(s * 10000) / 10000 ;
        return Double.parseDouble(df.format(s/1000));
    }
    
    public static double getDistance(String latlng1,String latlng2){
    	String[] latlngs1 = latlng1.split(",");
    	String[] latlngs2 = latlng2.split(",");
    	double lat1 = Double.parseDouble(latlngs1[0]);
    	double lng1 = Double.parseDouble(latlngs1[1]);
    	double lat2 = Double.parseDouble(latlngs2[0]);
    	double lng2 = Double.parseDouble(latlngs2[1]);
    	
    	return getDistance(lat1, lng1, lat2, lng2);
    }
    
    public static void main(String[] args) {
//        System.out.println(getDistance(31.215937,121.446014,31.2158502442799, 121.446028464238 ));
        System.out.println(getDistance("31.215937,121.446014","31.2158502442799, 121.446028464238"));
    }
}
