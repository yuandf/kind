package com.kind.perm.core.system.dao;


import java.util.List;

import com.kind.common.persistence.PageQuery;
import com.kind.perm.core.system.domain.AreaDO;

/**
 * Function:区域信息数据访问接口. <br/>
 * @date:2017-03-01 09:53:29 <br/>
 * @author 李明
 * @version:
 * @since:JDK 1.7
 */
public interface AreaDao {

    final String NAMESPACE = "com.kind.perm.core.mapper.system.AreaDOMapper.";

	/**
	 * 分页查询的数据<br/>
	 *
	 * @param pageQuery
	 * @return
	 */
	List<AreaDO> page(PageQuery pageQuery);

	/**
	 * 分页查询的数据记录数<br/>
	 *
	 * @param pageQuery
	 * @return
	 */
	int count(PageQuery pageQuery);

	/**
	 * [保存/修改] 数据<br/>
	 *
	 * @param entity
	 */
	int saveOrUpdate(AreaDO entity);

	/**
	 * 根据id获取数据对象<br/>
	 *
	 * @param id
	 * @return
	 */
	AreaDO getById(Long id);

    /**
     * 删除数据 <br/>
     *
     * @param id
     */
    void remove(Long id);
    
	/**
	 * 查询符合条件的所有数据
	 * @param entity
	 * @return
	 */
	List<AreaDO> queryList(AreaDO entity);
	
	/**
     * 根据Pcode 查询
     * @param pcode
     * @return
     */
	List<AreaDO> queryByPcode(String pcode);

	/**
     * 根据code 查询
     * @param code
     * @return
     */
	AreaDO queryByCode(String code);
}
