package com.kind.perm.core.demo.dao.impl;

import java.io.Serializable;
import java.util.List;

import org.springframework.stereotype.Repository;

import com.kind.common.datasource.KindDataSourceHolder;
import com.kind.common.datasource.DataSourceKey;
import com.kind.common.persistence.PageQuery;
import com.kind.common.persistence.mybatis.BaseDaoMyBatisImpl;
import com.kind.perm.core.demo.dao.CommunityDao;
import com.kind.perm.core.demo.domain.CommunityDO;

/**
 * 
 * 小区数据访问实现类. <br/>
 * 
 * @date:2016年12月11日 <br/>
 * @author 李明
 * @version:
 * @since:JDK 1.7
 */
@Repository
public class CommunityDaoImpl extends BaseDaoMyBatisImpl<CommunityDO, Serializable> implements CommunityDao {

	@Override
	public List<CommunityDO> page(PageQuery pageQuery) {
		return super.query(NAMESPACE + "page", pageQuery);
	}

	@Override
	public int count(PageQuery pageQuery) {
		return super.count(NAMESPACE + "count", pageQuery);
	}

	@Override
	public int saveOrUpdate(CommunityDO entity) {
		KindDataSourceHolder.setDataSourceKey(DataSourceKey.YM_ORDER_DATA_SOURCE);
		if (entity.getId() == null) {
			return super.insert(NAMESPACE + "insert", entity);
		} else {
			return super.update(NAMESPACE + "update", entity);
		}
	}

	@Override
	public void remove(Long id) {
		delete(NAMESPACE + "delete", id);
	}

	@Override
	public CommunityDO getById(Long id) {
		return super.getById(NAMESPACE + "getById", id);
	}

	@Override
	public List<CommunityDO> queryList(CommunityDO entity) {
		return super.query(NAMESPACE + "queryList", entity);
	}

}
