package com.kind.perm.core.system.domain;



import com.kind.common.persistence.PageQuery;

/**

 * 机构信息<br/>

 *

 * @Date: 2017-03-02 11:12:13

 * @author 李明

 * @version

 * @since JDK 1.7

 * @see

 */

public class OrganizationDO extends PageQuery {

	

	
	/** 主键id*/
	private java.lang.Integer id;
	/** 部门名称*/
	private java.lang.String orgName;
	/** 关联的父级id*/
	private java.lang.Integer pid;
	/** 属于的类型*/
	private java.lang.String orgType;
	/** 排序*/
	private java.lang.Integer orgSort;
	/** 级别*/
	private java.lang.Integer orgLevel;
	/** 机构编码号*/
	private java.lang.String orgCode;
	/** 关联的区域id*/
	private java.lang.Integer areaId;
	public java.lang.Integer getId() {
	    return this.id;
	}
	public void setId(java.lang.Integer id) {
	    this.id=id;
	}
	public java.lang.String getOrgName() {
	    return this.orgName;
	}
	public void setOrgName(java.lang.String orgName) {
	    this.orgName=orgName;
	}
	public java.lang.Integer getPid() {
	    return this.pid;
	}
	public void setPid(java.lang.Integer pid) {
	    this.pid=pid;
	}
	public java.lang.String getOrgType() {
	    return this.orgType;
	}
	public void setOrgType(java.lang.String orgType) {
	    this.orgType=orgType;
	}
	public java.lang.Integer getOrgSort() {
	    return this.orgSort;
	}
	public void setOrgSort(java.lang.Integer orgSort) {
	    this.orgSort=orgSort;
	}
	public java.lang.Integer getOrgLevel() {
	    return this.orgLevel;
	}
	public void setOrgLevel(java.lang.Integer orgLevel) {
	    this.orgLevel=orgLevel;
	}
	public java.lang.String getOrgCode() {
	    return this.orgCode;
	}
	public void setOrgCode(java.lang.String orgCode) {
	    this.orgCode=orgCode;
	}
	public java.lang.Integer getAreaId() {
	    return this.areaId;
	}
	public void setAreaId(java.lang.Integer areaId) {
	    this.areaId=areaId;
	}

}



