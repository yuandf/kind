package com.kind.perm.core.system.service;

import java.util.List;

import com.kind.common.persistence.PageQuery;
import com.kind.common.persistence.PageView;
import com.kind.perm.core.system.domain.OrganizationDO;

/**
 * 机构信息业务处理接口<br/>
 *
 * @Date: 2017-03-02 09:24:35
 * @author 李明
 * @version
 * @since JDK 1.7
 * @see
 */
public interface OrganizationService {

    /**
     * 分页查询
     * @param pageQuery
     * @return
     */
	PageView<OrganizationDO> selectPageList(PageQuery pageQuery);

    /**
     * 保存数据
     * @param entity
     */
	int save(OrganizationDO entity);

    /**
     * 获取数据对象
     * @param id
     * @return
     */
    OrganizationDO getById(Long id);

    /**
     * 删除数据
     * @param id
     */
	void remove(Long id);
	
	/**
	 * 查询符合条件的所有数据
	 * @param entity
	 * @return
	 */
	List<OrganizationDO> queryList(OrganizationDO entity);
	
}
