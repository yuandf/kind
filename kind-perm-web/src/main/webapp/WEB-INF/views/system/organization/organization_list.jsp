<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
<title></title>
<%@ include file="/WEB-INF/views/include/easyui.jsp"%>
<script src="${ctx}/static/plugins/My97DatePicker/WdatePicker.js" type="text/javascript"></script>
</head>

<body>
    <div id="tb" style="padding:5px;height:auto">
        <div>
            <form id="searchFrom" action="">
             <input type="hidden" name="order"/>
             <input type="hidden" name="sort"/>
<!-- 				<input type="text" name="orgName" class="easyui-validatebox" data-options="width:150,prompt: '部门名称'"/> -->
<!-- 				<input type="text" name="pid" class="easyui-validatebox" data-options="width:150,prompt: '关联的父级id'"/> -->
<!-- 				<input type="text" name="orgType" class="easyui-validatebox" data-options="width:150,prompt: '属于的类型'"/> -->
<!-- 				<input type="text" name="orgSort" class="easyui-validatebox" data-options="width:150,prompt: '排序'"/> -->
<!-- 				<input type="text" name="orgLevel" class="easyui-validatebox" data-options="width:150,prompt: '级别'"/> -->
<!-- 				<input type="text" name="orgCode" class="easyui-validatebox" data-options="width:150,prompt: '机构编码号'"/> -->
<!-- 				<input type="text" name="areaId" class="easyui-validatebox" data-options="width:150,prompt: '关联的区域id'"/> -->
         	
<!--                 <span class="toolbar-item dialog-tool-separator"></span> -->
<!--                 <a href="javascript(0)" class="easyui-linkbutton" iconCls="icon-search" plain="true" onclick="searchFunc();">查询</a> -->
            </form>
            <shiro:hasPermission name="system:organization:save">
	            <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-add" plain="true" onclick="addFunc();">添加</a>
	            <span class="toolbar-item dialog-tool-separator"></span>
            </shiro:hasPermission>
            <shiro:hasPermission name="system:organization:change">
	            <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-edit" plain="true" onclick="updateFunc();">修改</a>
	            <span class="toolbar-item dialog-tool-separator"></span>
            </shiro:hasPermission>
            <shiro:hasPermission name="system:organization:view">
	            <a href="#" class="easyui-linkbutton" plain="true" iconCls="icon-search" onclick="view()">查看</a>
			    <span class="toolbar-item dialog-tool-separator"></span>
		    </shiro:hasPermission>
		    <shiro:hasPermission name="system:organization:remove">
	            <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-remove" plain="true" onclick="removeFunc();">删除</a>
	            <span class="toolbar-item dialog-tool-separator"></span>
            </shiro:hasPermission>
             <shiro:hasPermission name="system:organization:export">
	            <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-hamburg-home" plain="true" onclick="exportFunc();">导出</a>
	            <span class="toolbar-item dialog-tool-separator"></span>
            </shiro:hasPermission>
        </div>
    </div>

    <!--列表表格-->
    <table id="dg"></table>
    <!--添加、修改窗体-->
	<div id="dlg"></div> 
	<div id="icon_dlg"></div>  

<script type="text/javascript">
var dg;
var d;

var parentPermId;
$(function(){   
	dg=$('#dg').treegrid({  
	method: "get",
    url:'${ctx}/system/organization/json', 
    fit : true,
	fitColumns : true,
	border : false,
	idField : 'id',
	treeField:'orgName',
	parentField : 'pid',
	iconCls: 'icon',
	animate:true, 
	rownumbers:true,
	singleSelect:true,
	striped:true,
    columns:[[
		{field:'ck',checkbox:true },
        {field:'id',title:'id',hidden:true},    
        {field:'orgName',title:'部门名称',width:'150'},
        {field:'orgLevel',title:'级别',width:'100'},
        {field:'orgCode',title:'机构编码号',width:'100'},
        {field:'orgType',title:'属于的类型',width:'100'},
        {field:'orgSort',title:'排序',width:'100'},
    ]],
    enableHeaderClickMenu: false,
    enableHeaderContextMenu: false,
    enableRowContextMenu: false,
    onLoadSuccess: function(data){
    	if(data){
    		$.each(data.rows,
    		function(index,item){
    		  if(item.checked){
    			$('#dg').datagrid('checkRow',index);
    		  }
    		});
    	}
    },
    toolbar:'#tb',
    dataPlain: true
	});
	
});

//弹窗增加
function addFunc() {
	//父级权限
	var row = dg.treegrid('getSelected');
	if(row){
		parentPermId=row.id;
	}
	
	d=$('#dlg').dialog({    
	    title: '添加机构信息',    
	    width: 1200,    
	    height: 900,    
	    closed: false,    
	    cache: false,
	    maximizable:true,
	    resizable:true,
	    href:'${ctx}/system/organization/add',
	    modal: true,
	    buttons:[{
			text:'确认',
			handler:function(){
				$("#mainform").submit();
			}
		},{
			text:'取消',
			handler:function(){
					d.panel('close');
				}
		}]
	});
}

//删除
function removeFunc(){
	var row = dg.treegrid('getSelected');
	if(rowIsNull(row)) return;
	parent.$.messager.confirm('提示', '删除后无法恢复您确定要删除？', function(data){
		if (data){
			$.ajax({
				type:'get',
				url:"${ctx}/system/organization/remove/"+row.id,
				success: function(data){
					if(successTip(data,dg))
			    		dg.treegrid('reload');
				}
			});
			//dg.datagrid('reload'); //grid移除一行,不需要再刷新
		} 
	});

}

//修改
function updateFunc(){
	var row = dg.treegrid('getSelected');
	if(rowIsNull(row)) return;
	//父级权限
	parentPermId=row.pid;
	d=$("#dlg").dialog({   
	    title: '修改机构信息',    
	    width: 1200,    
	    height: 900,    
	    href:'${ctx}/system/organization/update/'+row.id,
	    maximizable:true,
	    modal:true,
	    buttons:[{
			text:'确认',
			handler:function(){
				$("#mainform").submit();
			}
		},{
			text:'取消',
			handler:function(){
					d.panel('close');
				}
		}]
	});

}

var nowIcon;
var icon_dlg;
    /**
     *查看机构信息
     */
    function view(){
    	var row = dg.datagrid('getSelected');
    	if(rowIsNull(row)) return;
    	d=$("#dlg").dialog({   
    	    title: '查看机构信息',    
    	    width: 1200,    
    	    height: 900,    
    	    href:'${ctx}/system/organization/view/'+row.id,
    	    maximizable:true,
    	    modal:true,
    	    buttons:[]
    	});

    }
    
    //导出机构信息
    function exportFunc(){
    	$('#searchFrom').form('submit', {
    	    url:"${ctx}/system/organization/export",
    	    onSubmit: function(){
    			// do some check
    			// return false to prevent submit;
    	    },
    	    success:function(data){
    			
    	    }
    	});
    	 
    }

    //分页查询
    function searchFunc(){
        var formObj=$("#searchFrom").serializeObject();
        dg.treegrid('load',formObj);
    }

</script>
</body>
</html>