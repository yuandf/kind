package com.kind.perm.web.common;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;

public class ResourceUtil
{

  public static String FILE_PATH = "";

  static
  {
	  FILE_PATH = getFILE_PATH();
  }


  public  static final String getFILE_PATH()
  {
	  Properties properties=new Properties(); ;
	  try {
		  properties.load(new ResourceUtil().getClass().getResourceAsStream("/conf.properties"));
      } catch (FileNotFoundException e) {
          e.printStackTrace();
      } catch (IOException e) {
          e.printStackTrace();
      }
    return properties.getProperty("file_path");
  }


}